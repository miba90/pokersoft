/**
 * Created by michal on 10.05.16.
 */
import {Component} from "angular2/core";
import {HeadsUpService} from "./heads-up.service";
import {HTTP_PROVIDERS} from "angular2/http";

@Component({
    templateUrl: 'app/heads-up/heads-up.component.html',
    providers: [HeadsUpService, HTTP_PROVIDERS],
    selector: 'hu-action'
})
export class HeadsUpComponent{
    data : Object = {};
    constructor(private huService: HeadsUpService){
        huService.getAction().subscribe(data => this.data = data);
    }


}
