import {Injectable} from "angular2/core";
import {Http, Response} from "angular2/http";
import {Observable} from "rxjs/Observable";


@Injectable()
export class HeadsUpService{
    private apiAddress : string = 'http://localhost:5000';

    constructor(private http : Http){}

    getAction() : Observable<any>{
        let tmp =  this.http.get(this.apiAddress).map(this.extractData).catch(this.handleError);
        return tmp;
    }

    private extractData(res: Response) {
        if (res.status < 200 || res.status >= 300) {
            throw new Error('Bad response status: ' + res.status);
        }
        
        res.json
        
        let body = res.json();
        return body.data || { };
    }


    private handleError (error: any) {
        // In a real world app, we might send the error to remote logging infrastructure
        let errMsg = error.message || 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }
}
