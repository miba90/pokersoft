import re
from server.hand_context.basic_context import BasicContext
from server.hand_context.board import Board
from server.hand_context.decision import Decision, DecisionType
from server.hand_context.player import Player
from server.hand_context.street import Street


class Hm2ContextFactory:

    __idReg = re.compile(r".*? Hand History for Game ([0-9]*?) .*")
    __bbReg = re.compile(r".*?/\$(.*) Blinds No Limit Holdem .*")

    __seatReg = re.compile(r"Seat (.{1,2}): (.*?) \( \$(.*?) \)")
    __sbPostReg = re.compile(r"(.*?) posts small blind .*")
    __bbPostReg = re.compile(r"(.*?) posts big blind \[\$(.*?)\]")

    __dealtReg = re.compile(r"Dealt to (.*) \[ (.*) \]")
    __dealtStreetReg = re.compile(r"\*\* Dealing (.*?) \*\* \[ (.*) \]")

    __actionFoldReg = re.compile(r"(.*?) folds")
    __actionChecksReg = re.compile(r"(.*?) checks")
    __actionCallReg = re.compile(r"(.*?) calls \[\$(.*?)\]")
    __actionBetsReg = re.compile(r"(.*?) bets \[\$(.*?)\]")
    __actionRaisesReg = re.compile(r"(.*?) raises \[\$(.*?)\]")

    __showReg = re.compile(r"(.*) shows \[ (.*) \]")
    __resultsReg = re.compile(r"(.*?) collected \[ \$(.*?) \]")

    def __init__(self, actions_lines):
        self._context = BasicContext()
        self.actionsLines = actions_lines

    def create(self):
        for line in self.actionsLines:

            match = self.__idReg.match(line)
            if match:
                self.__id(match)
                continue

            match = self.__bbReg.match(line)
            if match:
                self.__bb(match)
                continue

            match = self.__seatReg.match(line)
            if match:
                self.__seat(match)
                continue

            match = self.__bbPostReg.match(line)
            if match:
                self.__bb_post(match)
                continue

            match = self.__dealtReg.match(line)
            if match:
                self.__dealt(match)
                continue

            match = self.__dealtStreetReg.match(line)
            if match:
                self.__dealt_street(match)
                continue

            match = self.__actionFoldReg.match(line)
            if match:
                self.__action_fold(match)
                continue

            match = self.__actionChecksReg.match(line)
            if match:
                self.__action_check(match)
                continue

            match = self.__actionCallReg.match(line)
            if match:
                self.__action_call(match)
                continue

            match = self.__actionRaisesReg.match(line) or self.__actionBetsReg.match(line)
            if match:
                self.__action_bet_raise(match)
                continue

            match = self.__showReg.match(line)
            if match:
                self.__show(match)
                continue

            match = self.__resultsReg.match(line)
            if match:
                self.__results(match)
                continue

        return self._context

    def __bb(self, match):
        self._context.bb = float(match.group(1))

    def __bb_post(self, match):
        self._context.players_collection[match.group(1)].is_bb = True

    def __seat(self, match):
        chips = float(match.group(3)) / self._context.bb
        player = Player(match.group(2), '', chips=chips, seat_num=int(match.group(1)))
        self._context.players_collection[match.group(2)] = player
        pass

    def __dealt(self, match):
        self._context.players_collection[match.group(1)].hand = match.group(2)

    def __dealt_street(self, match):
        self._context.street = self.__get_street(match.group(1))
        if self._context.street == Street.Flop:
            self._context.board = Board(match.group(2))
        else:
            self._context.board += match.group(2)

    def __action_fold(self, match):
        playerName = match.group(1)
        street = self._context.street
        self._context.decision_collection[street].append(Decision(playerName, DecisionType.Fold, 0))

    def __action_check(self, match):
        playerName = match.group(1)
        street = self._context.street
        self._context.decision_collection[street].append(Decision(playerName, DecisionType.CallCheck, 0))

    def __action_call(self, match):
        playerName = match.group(1)
        value = float(match.group(2)) / self._context.bb
        street = self._context.street
        self._context.decision_collection[street].append(Decision(playerName, DecisionType.CallCheck, value))

    def __action_bet_raise(self, match):
        playerName = match.group(1)
        value = float(match.group(2)) / self._context.bb
        street = self._context.street
        self._context.decision_collection[street].append(Decision(playerName, DecisionType.BetRaise, value))

    def __show(self, match):
        playerName = match.group(1)
        cards = match.group(2)
        self._context.show[playerName] = cards

    def __results(self, match):
        self._context.winner = match.group(1)

    @staticmethod
    def __get_street(str):
        if str == '':
            return Street.Preflop
        if str == 'flop':
            return Street.Flop
        if str == 'turn':
            return Street.Turn
        if str == 'river':
            return Street.River
        pass

    def __id(self, match):
        self._context.id = int(match.group(1))
