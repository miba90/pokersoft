from typing import List

import re
from os import listdir
from server.hand_context.basic_context import BasicContext
from server.parsers.hm2_context_factory import Hm2ContextFactory


class HandFileState:
    def __init__(self):
        pass
    Open, Closed = range(2)


class Hm2Parser:
    __regOpenHand = r"\** (.*?) Hand History for Game (\d*) \**"
    __regClosedHand = r"(.*?) collected \[ \$(.*?) \]"

    def __init__(self, root):
        self.root = root

    def get_contexts(self) -> List[BasicContext]:
        listDir = listdir(self.root)
        retDict = {}
        for x in listDir:
            fileElements = self.__extract_hands(x)
            for i in fileElements:
                retDict[i.id] = i
        return retDict

    def __extract_hands(self, filename) -> BasicContext:
        my_file = open('{}/{}'.format(self.root, filename))
        tmpLines = []
        with my_file as fileContent:
            handState = HandFileState.Closed
            handLines = []
            for line in fileContent:
                if handState == HandFileState.Closed:
                    match = re.search(self.__regOpenHand, line)
                    if match is not None:
                        print("New Hand added: {}".format(match.group(2)))
                        handState = HandFileState.Open
                if handState == HandFileState.Open:
                    handLines.append(line)
                    if re.match(self.__regClosedHand, line):
                        handState = HandFileState.Closed
                        tmpLines.append(handLines)
                        handLines = []
        retLines = []
        for _ in tmpLines:
            contextFactory = Hm2ContextFactory(_)
            context = contextFactory.create()
            retLines.append(context)
        return retLines
