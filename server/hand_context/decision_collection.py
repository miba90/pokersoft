from typing import List

from server.hand_context.decision import Decision
from server.hand_context.street import Street


class DecisionCollection:
    def __init__(self):
        decision_list = {
            Street.Preflop: [],
            Street.Flop: [],
            Street.Turn: [],
            Street.River: []
        }
        self.__decision_list = decision_list

    def __getitem__(self, item) -> List[Decision]:
        return self.__decision_list[item]

    def __setitem__(self, key, value: Decision):
        if not isinstance(value, Decision):
            raise ValueError("Argument must be Decision")
        self.__decision_list[key].append(value)
