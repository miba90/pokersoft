from server.hand_context.player import Player


class PlayersCollection:
    def __init__(self):
        self._collection = {}

    def __getitem__(self, item) -> Player:
        return self._collection.get(item)

    def __setitem__(self, key, value):
        if not isinstance(value, Player):
            raise ValueError()
        if key in self._collection:
            self._collection[key] = value
            return
        self._collection.update({key: value})

    def __len__(self):
        return len(self._collection)

    def __iter__(self):
        return iter(list(self._collection.values()))
