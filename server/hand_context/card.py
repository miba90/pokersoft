class Card:
    def __init__(self, card_string):
        self.type = card_string[0]
        self.color = card_string[1]

    def __str__(self):
        return '{}{}'.format(self.type, self.color)

    def __eq__(self, other):
        return str(self) == str(other)
