# noinspection PyClassHasNoInit
class DecisionType:
    Fold, CallCheck, BetRaise = range(3)


class Decision:
    def __init__(self, player_name, decision_type, value):
        self.player_name = player_name
        self.decision_type = decision_type
        self.value = value
