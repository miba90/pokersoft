import string

from server.hand_context.card import Card
from server.hand_context.street import Street


class Board:
    def __init__(self, cards_list=''):
        self.__cardsList = cards_list

    def __getitem__(self, item: Street):
        return self.get_board(item)

    def __iadd__(self, other):
        self.__cardsList = "{}, {}".format(self.__cardsList, other)
        return self

    def get_board(self, street: Street) -> string:
        if street == Street.Preflop:
            return ''
        cards = [Card(c) for c in self.__cardsList.split(', ')]
        if street == Street.Flop:
            return cards[:3]
        if street == Street.Turn:
            return cards[:4]
        if street == Street.River:
            return cards[:5]

    def append(self, card):
        self.__cardsList = "{}, {}".format(self.__cardsList, card)
        pass
