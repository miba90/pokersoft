from server.hand_context.board import Board
from server.hand_context.decision_collection import DecisionCollection
from server.hand_context.players_collection import PlayersCollection
from server.hand_context.street import Street


class BasicContext:

    def __init__(self,
                 hand_id=None):
        self.id = hand_id
        self.board = Board()  # type: Board
        self.players_collection = PlayersCollection()  # type: PlayersCollection
        self.decision_collection = DecisionCollection()  # type: DecisionCollection
        self.street = Street.Preflop  # type: Street
        self.bb = 0
        self.show = {}
        self.winner = None
