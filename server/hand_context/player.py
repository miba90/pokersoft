import string


class Player:
    def __init__(self, name='', hand='', chips=None, seat_num=None, is_bb=None):
        self.hand = hand  # type: string
        self.name = name  # type: string
        self.chips = chips  # type: float
        self.seat_num = seat_num  # type: int
        self.is_bb = is_bb  # type: bool
