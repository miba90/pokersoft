from flask import Flask, url_for
from flask.ext.cors import CORS


app = Flask(__name__)
CORS(app)

@app.route('/')
def api_root():
    return '{"data": {"msg": "Welcome"}}'

@app.route('/articles')
def api_articles():
    return 'List of ' + url_for('api_articles')

@app.route('/articles/<articleid>')
def api_article(articleid):
    return 'You are reading ' + articleid

if __name__ == '__main__':
    app.run()
