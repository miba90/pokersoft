from unittest import TestCase

from server.hand_context.basic_context import BasicContext
from server.hand_context.board import Board
from server.hand_context.player import Player
from server.hand_context.street import Street


class TestHandContext(TestCase):
    def test_PlayersCollection(self):
        p1 = 'p1'
        context = BasicContext(1)
        context.players_collection[p1] = Player(p1)
        self.assertEqual(context.players_collection[p1].name, p1, 'Nazwy graczy sa rozne')

    def test_Board(self):
        board = Board("2c, 3d, 4h")
        self.assertEqual(board.get_board(Street.Flop), ['2c', '3d', '4h'])
        board.append('5s')
        self.assertEqual(board.get_board(Street.Flop), ['2c', '3d', '4h'])
        self.assertEqual(board.get_board(Street.Turn), ['2c', '3d', '4h', '5s'])
        board.append('As')
        self.assertEqual(board.get_board(Street.Flop), ['2c', '3d', '4h'])
        self.assertEqual(board.get_board(Street.Turn), ['2c', '3d', '4h', '5s'])
        self.assertEqual(board.get_board(Street.River), ['2c', '3d', '4h', '5s', 'As'])
