from unittest import TestCase

import os
from server.hand_context.decision import DecisionType
from server.hand_context.street import Street
from server.parsers.hm2 import Hm2Parser


class TestHm2ContextFactory(TestCase):
    def setUp(self):
        directory = "{}/test_files".format(os.getcwd())
        parser = Hm2Parser(directory)
        self.contexts = parser.get_contexts()
        pass

    def test_Hand_877710249(self):
        context = self.contexts[877710249]

        self.assertEqual(context.bb, 0.3, 'Bad bb sizing value')
        self.assertEqual(len(context.players_collection), 6, 'Bad PlayersCollection size')

        self.assertListEqual(sorted(['rexus1', 'sharpp14', 'G00RE', 'Megga115', 'andychoncoa', 'Aggroski']),
                             sorted([p.name for p in context.players_collection]), 'Bad PlayersCollection')

        self.assertEqual(1, context.players_collection['rexus1'].seat_num, 'Wrong player position')
        self.assertEqual('7h, Th', context.players_collection['rexus1'].hand)

        self.assertListEqual([DecisionType.Fold, DecisionType.CallCheck, DecisionType.CallCheck, DecisionType.BetRaise,
                              DecisionType.Fold, DecisionType.Fold, DecisionType.CallCheck, DecisionType.CallCheck],
                             [a.decision_type for a in context.decision_collection[Street.Preflop]],
                             'Bad types in DecisionCollection (PREFLOP)')

        self.assertListEqual([0, 1, 1, 4, 0, 0, 3, 3],
                             [a.value for a in context.decision_collection[Street.Preflop]],
                             'Bad values in DecisionCollection (PREFLOP)')

    def test_Hand_877710295(self):
        context = self.contexts[877710295]
        bb = 0.3

        self.assertListEqual([DecisionType.BetRaise, DecisionType.CallCheck, DecisionType.Fold, DecisionType.Fold,
                              DecisionType.Fold, DecisionType.Fold],
                             [a.decision_type for a in context.decision_collection[Street.Preflop]],
                             'Bad types in DecisionCollection (PREFLOP)')

        self.assertListEqual([0.86 / bb, 0.86 / bb, 0, 0, 0, 0],
                             [a.value for a in context.decision_collection[Street.Preflop]],
                             'Bad values in DecisionCollection (PREFLOP)')

        self.assertListEqual([DecisionType.BetRaise, DecisionType.CallCheck],
                             [a.decision_type for a in context.decision_collection[Street.Flop]],
                             'Bad types in DecisionCollection (FLOP)')

        self.assertListEqual([1.62 / bb, 1.62 / bb],
                             [a.value for a in context.decision_collection[Street.Flop]],
                             'Bad values in DecisionCollection (FLOP)')

        self.assertListEqual([DecisionType.CallCheck, DecisionType.BetRaise, DecisionType.CallCheck],
                             [a.decision_type for a in context.decision_collection[Street.Turn]],
                             'Bad types in DecisionCollection (TURN)')

        self.assertListEqual([0, 4.05 / bb, 4.05 / bb],
                             [a.value for a in context.decision_collection[Street.Turn]],
                             'Bad values in DecisionCollection (TURN)')

        self.assertListEqual([DecisionType.CallCheck, DecisionType.BetRaise, DecisionType.Fold],
                             [a.decision_type for a in context.decision_collection[Street.River]],
                             'Bad types in DecisionCollection (RIVER)')

        self.assertListEqual([0, 8.25 / bb, 0],
                             [a.value for a in context.decision_collection[Street.River]],
                             'Bad values in DecisionCollection (RIVER)')

        self.assertEqual(['Ah', '4h', 'Ts'], context.board[Street.Flop], 'Wrong flop')
        self.assertEqual(['Ah', '4h', 'Ts', '5h'], context.board[Street.Turn], 'Wrong turn')
        self.assertEqual(['Ah', '4h', 'Ts', '5h', '2c'], context.board[Street.River], 'Wrong river')

        pass
